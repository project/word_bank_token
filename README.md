Description
-----------

Sometimes for texts, for example SEO texts, we need to use different synonyms
of the word. For example, the word "company" can have synonyms: company,
firm, agency, organization, ...
You can create token `[word_bank:company]` with a set of synonyms. And in the
text it will be replaced with one of the synonyms. Tokens stores as config entities.
This has some advantages when you need to deploy tokens using the config management.
You can also use `[a]` token of undefined article in front of WordBank token.
Its value will be changed depending on the value of the token.

Usage
-----

* Visit the `/admin/structure/word-bank-token` and add tokens.
* Replacing tokens with values can be done in two ways:
  1) Use setting _Preprocess WordBank token in field widget_ in the text fields settings.
  2) Use _Replace WordBank token_ filter in text format.

Maintainers
-----------
 * Alexandr Tarasenko (author): https://www.drupal.org/u/alextars
