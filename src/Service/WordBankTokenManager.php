<?php

namespace Drupal\word_bank_token\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * WordBank token handler.
 *
 * TokenAPI still uses procedural code, but we have moved it to a class for
 * easier refactoring.
 */
class WordBankTokenManager {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a WordBankTokenManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $translation;
  }

  /**
   * Implements hook_token_info().
   */
  public function getTokenInfo() {
    // WordBank token types.
    $info['types']['word_bank'] = [
      'name' => $this->t('WordBank'),
      'description' => $this->t('Tokens base on word bank configuration entities.'),
    ];

    foreach ($this->getEnabledWordBankTokensIds('word_bank_token') as $token) {
      $info['tokens']['word_bank'][$token] = [
        'name' => $this->t('Random phrase for "%token"', ['%token' => $token]),
        'description' => $this->t("Random phrase from word bank"),
      ];
    }

    return $info;
  }

  /**
   * Implements hook_tokens().
   */
  public function getTokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
    $replacements = [];

    if ($type == 'word_bank') {
      foreach ($tokens as $name => $original) {
        $token_entity = $this->entityTypeManager->getStorage('word_bank_token')->load($name);
        if ($token_entity) {
          $replacements[$original] = $token_entity->getRandomValue();
          $bubbleable_metadata->addCacheableDependency($token_entity);
        }
      }
    }

    return $replacements;
  }

  /**
   * Get enabled WordBank token ids.
   *
   * @return array|int
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEnabledWordBankTokensIds() {
    $storage = $this->entityTypeManager->getStorage('word_bank_token');
    return $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', TRUE)
      ->execute();
  }

}
