<?php

namespace Drupal\word_bank_token\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class WordBankTokenController.
 *
 * @package Drupal\word_bank_token\Controller
 */
class WordBankTokenController extends ControllerBase {

  /**
   * Enable token.
   *
   * @param string $word_bank_token
   *   Token ID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *    The RedirectResponse.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function enableEntity($word_bank_token) {
    $entity = $this->entityTypeManager()->getStorage('word_bank_token')->load($word_bank_token);
    $entity->set('status', TRUE);
    $entity->save();

    return $this->redirect('entity.word_bank_token.collection');
  }

  /**
   * Disable token.
   *
   * @param string $word_bank_token
   *   Token ID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The RedirectResponse.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function disableEntity($word_bank_token) {
    $entity = $this->entityTypeManager()->getStorage('word_bank_token')->load($word_bank_token);
    $entity->set('status', FALSE);
    $entity->save();

    return $this->redirect('entity.word_bank_token.collection');
  }

}
