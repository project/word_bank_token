<?php

namespace Drupal\word_bank_token\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the WordBank token entity.
 *
 * @ConfigEntityType(
 *   id = "word_bank_token",
 *   label = @Translation("WordBank token setting"),
 *   handlers = {
 *     "list_builder" = "Drupal\word_bank_token\WordBankTokenListBuilder",
 *     "form" = {
 *       "add" = "Drupal\word_bank_token\Form\WordBankTokenForm",
 *       "edit" = "Drupal\word_bank_token\Form\WordBankTokenForm",
 *       "delete" = "Drupal\word_bank_token\Form\WordBankTokenDeleteForm"
 *     },
 *   },
 *   config_prefix = "word_bank_token",
 *   admin_permission = "administer wordbank tokens",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/word-bank-token/add",
 *     "edit-form" = "/admin/structure/word-bank-token/{word_bank_token}/edit",
 *     "delete-form" = "/admin/structure/word-bank-token/{word_bank_token}/delete",
 *     "enable" = "/admin/structure/word-bank-token/{word_bank_token}/enable",
 *     "disable" = "/admin/structure/word-bank-token/{word_bank_token}/disable",
 *     "collection" = "/admin/structure/word-bank-token"
 *   },
 *   config_export = {
 *     "id",
 *     "values",
 *     "status",
 *   }
 * )
 */
class WordBankToken extends ConfigEntityBase implements WordBankTokenInterface {

  /**
   * The WordBank Token ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Array of values.
   *
   * @var string[]
   */
  protected $values;

  /**
   * The status, whether to be used by default.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getValues() {
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function getRandomValue() {
    $rand_keys = array_rand($this->values);
    return $this->values[$rand_keys];
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->status;
  }

}
