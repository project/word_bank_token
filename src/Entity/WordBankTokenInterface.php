<?php

namespace Drupal\word_bank_token\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining WordBankToken entity setting.
 */
interface WordBankTokenInterface extends ConfigEntityInterface {

  /**
   * Get values.
   *
   * @return array
   *   All values.
   */
  public function getValues();

  /**
   * Get random value for token.
   *
   * @return string
   *   Random Value.
   */
  public function getRandomValue();

  /**
   * Check if token is active.
   *
   * @return bool
   *   Status.
   */
  public function isActive();

}
