<?php

namespace Drupal\word_bank_token\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WordBankTokenForm.
 *
 * @package Drupal\word_bank_token\Form
 */
class WordBankTokenForm extends EntityForm {

  /**
   * The WordBank token storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * WordBankTokenForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   */
  public function __construct(EntityStorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('word_bank_token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Token machine-readable name'),
      '#default_value' => $this->entity->id(),
      '#description' => $this->t('A unique machine-readable name without "[" and "]". Can only contain lowercase letters, numbers, and underscores.'),
      '#machine_name' => [
        'exists' => 'Drupal\word_bank_token\Entity\WordBankToken::load',
      ],
      '#required' => FALSE,
    ];

    if (!$num_values = $form_state->get('num_values')) {
      $values = $this->entity->getValues();
      $num_values = $values === NULL ? 1 : count($values) + 1;
      $form_state->set('num_values', $num_values);
    }

    $form['#tree'] = TRUE;
    $form['values_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Values'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_values; $i++) {
      $form['values_fieldset']['value'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Value'),
      ];
      if (isset($values[$i])) {
        $form['values_fieldset']['value'][$i]['#default_value'] = $values[$i];
      }
    }

    $form['values_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['values_fieldset']['actions']['add_value'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];
    // If there is more than one name, add the remove button.
    if ($num_values > 1) {
      $form['values_fieldset']['actions']['remove_value'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addMoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $this->entity->isActive(),
    ];

    return $form;
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $num_values = $form_state->get('num_values');
    $form_state->set('num_values', $num_values + 1);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $num_values = $form_state->get('num_values');
    if ($num_values > 1) {
      $form_state->set('num_values', $num_values - 1);
    }
    $form_state->setRebuild();
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['values_fieldset'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue(['values_fieldset', 'value']);
    foreach ($values as &$value) {
      $value = trim($value);
    }
    $form_state->setValue('values', array_filter($values));

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created WordBank token %id', [
          '%id' => $this->entity->id(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved WordBank token %id', [
          '%id' => $this->entity->id(),
        ]));
    }
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

}
