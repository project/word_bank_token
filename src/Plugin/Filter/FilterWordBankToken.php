<?php

namespace Drupal\word_bank_token\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to replace WordBank token.
 *
 * @Filter(
 *   id = "filter_word_bank_token",
 *   title = @Translation("Replace WordBank token"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterWordBankToken extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    return new FilterProcessResult(word_bank_token_replace($text));
  }

}
