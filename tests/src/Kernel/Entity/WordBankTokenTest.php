<?php

namespace Drupal\Tests\word_bank_token\Kernel\Entity;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\word_bank_token\Entity\WordBankToken;

/**
 * @coversDefaultClass \Drupal\word_bank_token\Entity\WordBankToken
 * @group word_bank_token
 */
class WordBankTokenTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'word_bank_token',
  ];

  /**
   * @covers ::id
   * @covers ::getType
   * @covers ::getValues
   * @covers ::getRandomValue
   * @covers ::isActive
   */
  public function testWordBankToken() {
    $values = [
      'id' => 'test_id',
      'values' => ['value_a', 'value_b'],
      'status' => TRUE,
    ];

    $clutch_token = WordBankToken::create($values);
    $this->assertEquals('test_id', $clutch_token->id());
    $this->assertEquals(['value_a', 'value_b'], $clutch_token->getValues());
    $this->assertTrue($clutch_token->isActive());
    $this->assertThat($clutch_token->getRandomValue(), $this->logicalOr(
      $this->equalTo('value_a'),
      $this->equalTo('value_b')
    ));
  }

}
